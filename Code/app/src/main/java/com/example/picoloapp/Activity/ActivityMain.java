package com.example.picoloapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.picoloapp.R;

public class ActivityMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("com.example.picoloapp", "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prevention);
        preventionMenu();
    }

    @Override
    protected void onStart() {
        Log.d("com.example.picoloapp", "onStart()");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d("com.example.picoloapp", "onResume()");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d("com.example.picoloapp", "onPause()");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d("com.example.picoloapp", "onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("com.example.picoloapp", "onDestroy()");
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d("com.example.picoloapp", "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }

    private void preventionMenu(){
        final Button button_annuler = findViewById(R.id.button_annuler);
        final Button button_continuer = findViewById(R.id.button_continuer);

        button_annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        button_continuer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMain.super.getApplication(), ActivityMenu.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
