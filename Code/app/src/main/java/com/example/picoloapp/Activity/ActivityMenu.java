package com.example.picoloapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;

import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentChallenge;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentFinish;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentGame;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentLastGame;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentMenu;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentPledge;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentScore;
import com.example.picoloapp.Fragment.ChallengeFragment;
import com.example.picoloapp.Fragment.FinishFragment;
import com.example.picoloapp.Fragment.GameFragment;
import com.example.picoloapp.Fragment.LastGameFragment;
import com.example.picoloapp.Fragment.MenuFragment;
import com.example.picoloapp.Fragment.PledgeFragment;
import com.example.picoloapp.Fragment.ScoreFragment;
import com.example.picoloapp.R;
import com.example.picoloapp.Utils.StaticValues;

public class ActivityMenu extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener, IFragmentGame, IFragmentScore, IFragmentChallenge, IFragmentPledge, IFragmentFinish, IFragmentMenu, IFragmentLastGame {
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_menu);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if(fragmentManager.getFragments().isEmpty()){
            MenuFragment fragment = new MenuFragment();
            fragmentTransaction.add(R.id.main_fragment, fragment);
            fragmentTransaction.commit();
        }
        shouldDisplayHomeUp();
    }

    @Override
    protected void onStart() {
        Log.d(StaticValues.TAG, "onStart()");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(StaticValues.TAG, "onResume()");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(StaticValues.TAG, "onPause()");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(StaticValues.TAG, "onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(StaticValues.TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(StaticValues.TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    private void shouldDisplayHomeUp(){
        boolean canGoBack = getSupportFragmentManager().getBackStackEntryCount()>0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canGoBack);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    public void fragmentGame(){
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        GameFragment gameFragment = new GameFragment();

        fragmentTransaction.replace(R.id.main_fragment, gameFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void fragmentScore() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        ScoreFragment scoreFragment = new ScoreFragment();

        fragmentTransaction.replace(R.id.main_fragment, scoreFragment).addToBackStack("Score");
        fragmentTransaction.commit();
    }

    @Override
    public void fragmentChallenge() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        ChallengeFragment challengeFragment = new ChallengeFragment();

        fragmentTransaction.replace(R.id.main_fragment, challengeFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void fragmentPledge() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        PledgeFragment pledgeFragment = new PledgeFragment();

        fragmentTransaction.replace(R.id.main_fragment, pledgeFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void fragmentFinish() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        FinishFragment finishFragment = new FinishFragment();

        fragmentTransaction.replace(R.id.main_fragment, finishFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void fragmentMenu() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        MenuFragment menuFragment = new MenuFragment();

        fragmentTransaction.replace(R.id.main_fragment, menuFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void fragmentLastGame() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        LastGameFragment lastGameFragment = new LastGameFragment();

        fragmentTransaction.replace(R.id.main_fragment, lastGameFragment).addToBackStack("LastGame");
        fragmentTransaction.commit();
    }
}
