package com.example.picoloapp.Activity;


import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.picoloapp.R;

public class ActivitySettings  extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Faire la gestion du mode sombre ou mode clair (Déjà dispo pour les versions d'android qui on cette fonctionnalité)
        // Faire le parametrage des langues en dur (déjà dispo en changeant la langue de base de l'appareil)

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    @Override
    protected void onStart() {
        Log.d("com.example.picoloapp", "onStart()");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d("com.example.picoloapp", "onResume()");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d("com.example.picoloapp", "onPause()");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d("com.example.picoloapp", "onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("com.example.picoloapp", "onDestroy()");
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d("com.example.picoloapp", "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }
}
