package com.example.picoloapp.Bibliotheque.Interfaces;

import android.bluetooth.BluetoothSocket;

public interface IBluetoothConnection {
    void manageMyConnectedSocket(BluetoothSocket socket);
}
