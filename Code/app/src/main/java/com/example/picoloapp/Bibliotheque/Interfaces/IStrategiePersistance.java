package com.example.picoloapp.Bibliotheque.Interfaces;

import java.util.ArrayList;

public interface IStrategiePersistance {
    ArrayList<IAction> loadActions();
    void saveActions(ArrayList<IAction> actions);
}
