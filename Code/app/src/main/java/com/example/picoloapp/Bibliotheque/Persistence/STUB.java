package com.example.picoloapp.Bibliotheque.Persistence;

import com.example.picoloapp.Bibliotheque.Interfaces.IAction;
import com.example.picoloapp.Bibliotheque.Interfaces.IStrategiePersistance;
import com.example.picoloapp.Metier.GameManager.Action.Challenge;
import com.example.picoloapp.Metier.GameManager.Action.Pledge;
import com.example.picoloapp.Metier.GameManager.Action.Question;

import java.util.ArrayList;

public class STUB implements IStrategiePersistance {
    private ArrayList<IAction> listAction = new ArrayList<IAction>();

    @Override
    public ArrayList<IAction> loadActions() {
        Question question = new Question("Tu préfére être un chien ou un chat ?");
        question.addAnswer("Un chien");
        question.addAnswer("Un chat");

        Question question2 = new Question("Tu préfére être invisible ou pouvoir te teleporter ?");
        question2.addAnswer("Invisible");
        question2.addAnswer("Teleporter");
        listAction.clear();
        listAction.add(new Challenge("Tu dois faire 1 pompe"));
        listAction.add(new Challenge("Tu dois faire 2 minutes de gainage"));
        listAction.add(new Challenge("Tu dois faire 3 abdos"));
        listAction.add(new Challenge("Tu dois faire 4 burpies"));
        listAction.add(new Challenge("Tu dois faire 5 squats"));
        //listAction.add(question);
        //listAction.add(question2);
        listAction.add(new Pledge());
        listAction.add(new Pledge());
        listAction.add(new Pledge());

        return listAction;
    }

    @Override
    public void saveActions(ArrayList<IAction> actions) {

    }
}
