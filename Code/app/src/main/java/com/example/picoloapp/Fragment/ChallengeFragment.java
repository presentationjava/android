package com.example.picoloapp.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentGame;
import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.Metier.GameManager.Action.Challenge;
import com.example.picoloapp.R;
import com.example.picoloapp.Utils.StaticValues;
import com.example.picoloapp.Utils.Enumeration.GameStatus;

public class ChallengeFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_challenge, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        creationId();
    }

    private void creationId() {
        final TextView textView = getView().findViewById(R.id.textView_challenge);
        final Button button_validate = getView().findViewById(R.id.button_challenge_done);
        final Button button_cancel = getView().findViewById(R.id.button_challenge_cancel);
        Challenge challenge = (Challenge) Game.getInstance().getActionSelected();
        textView.setText(Game.getInstance().getPlayerSelected().getName() + "\n" + challenge.getDescription());
        button_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().getPlayerSelected().setScore(Game.getInstance().getPlayerSelected().getScore() + StaticValues.SCORE_CHALLENGE_VALID);
                closeFragment();
            }
        });
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().getPlayerSelected().setScore(Game.getInstance().getPlayerSelected().getScore() + StaticValues.SCORE_CHALLENGE_FAILED);
                closeFragment();
            }
        });
    }

    private void closeFragment() {
        try {
            Game.getInstance().setGameStatus(GameStatus.RUN);
            Game.getInstance().isFinish();
            ((IFragmentGame) getActivity()).fragmentGame();
        } catch (ClassCastException e) { }
    }
}
