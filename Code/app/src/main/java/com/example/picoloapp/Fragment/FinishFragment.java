package com.example.picoloapp.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentMenu;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentScore;
import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.R;
import com.example.picoloapp.Utils.StaticValues;
import com.google.gson.Gson;

public class FinishFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_finish, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        creationId();
    }

    private void creationId() {
        TextView textView_winner = getView().findViewById(R.id.textView_winner);
        TextView textView_loser = getView().findViewById(R.id.textView_loser);
        TextView textView_number = getView().findViewById(R.id.textView_number_gorged);
        final Button button_replay = getView().findViewById(R.id.button_replay);
        final Button button_score = getView().findViewById(R.id.button_view_score);
        final Button button_stop = getView().findViewById(R.id.button_stop);

        Game.getInstance().sortListPlayer();
        textView_winner.setText(textView_winner.getText() + Game.getInstance().getListPlayer().get(0).getName());
        textView_loser.setText(textView_loser.getText() + Game.getInstance().getListPlayer().get(Game.getInstance().getListPlayer().size() - 1).getName());
        int number = Game.getInstance().getListPlayer().get(0).getScore() - Game.getInstance().getListPlayer().get(Game.getInstance().getListPlayer().size() - 1).getScore();
        textView_number.setText(textView_number.getText() + String.valueOf(number));

        button_replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
                Game.getInstance().clearInstance();
                closeFragment();
            }
        });
        button_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentScore();
            }
        });
        button_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
                getActivity().finish();
            }
        });
    }

    private void save(){
        SharedPreferences sharedPreferencesName = getContext().getSharedPreferences(StaticValues.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferencesPlayers = getContext().getSharedPreferences(StaticValues.SHARED_PREFERENCES_PLAYERS, Context.MODE_PRIVATE);

        Gson gson = new Gson();
        Game.getInstance().save();

        sharedPreferencesName.edit().putString("name", Game.getInstance().getGameSave().getName()).apply();
        sharedPreferencesPlayers.edit().putString("players", gson.toJson(Game.getInstance().getGameSave().getPlayers())).apply();
    }

    private void fragmentScore() {
        try {
            ((IFragmentScore) getActivity()).fragmentScore();
        } catch (ClassCastException e) { }
    }

    private void closeFragment() {
        try {
            ((IFragmentMenu) getActivity()).fragmentMenu();
        } catch (ClassCastException e) { }
    }
}
