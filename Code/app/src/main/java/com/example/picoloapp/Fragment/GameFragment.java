package com.example.picoloapp.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentChallenge;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentFinish;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentLastGame;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentPledge;
import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentScore;
import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.R;
import com.example.picoloapp.Utils.Enumeration.GameStatus;

public class GameFragment extends Fragment {
    private TextView textView_error;
    private TextView textView_add_player;
    private EditText editText_add_player;
    private Button button_add_player;
    private Button button_start;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        creationId();
    }

    @Override
    public void onStart() {
        Game.getInstance().isFinish();
        if(Game.getInstance().getGameStatus() == GameStatus.RUN){
            nextAction();
        }
        super.onStart();
    }

    private void creationId() {
        final LinearLayout layout_player = getView().findViewById(R.id.linearLayout_player);
        textView_error = getView().findViewById(R.id.textView_error);
        textView_add_player = getView().findViewById(R.id.textView_add_player);
        editText_add_player = getView().findViewById(R.id.editText_player);
        button_add_player = getView().findViewById(R.id.button_add_player);
        button_start = getView().findViewById(R.id.button_start);
        final Button button_last_game = getView().findViewById(R.id.button_last_game);
        final Button button_score = getView().findViewById(R.id.button_view_score);
        controleStatus();
        button_add_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_player.addView(addNewPlayer(editText_add_player.getText().toString()));
                editText_add_player.setText("");
            }

            private TextView addNewPlayer(String playerName) {
                TextView newTextView = new TextView(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                newTextView.setLayoutParams(layoutParams);
                if(TextUtils.isEmpty(playerName)){
                    writeError(getString(R.string.game_player_name_missing));
                    return newTextView;
                }
                if(Game.getInstance().addPlayer(playerName)){
                    newTextView.setText(playerName);
                    clearError();
                    return newTextView;
                }
                writeError(getString(R.string.game_player_name_exist));
                return newTextView;
            }
        });
        button_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().startGame();
                if(Game.getInstance().getGameStatus() == GameStatus.RUN){
                    nextAction();
                }
            }
        });
        button_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentScore();
            }
        });
        button_last_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentLastGame();
            }
        });
    }

    private void controleStatus() {
        switch(Game.getInstance().getGameStatus()){
            case RUN:
                textView_add_player.setVisibility(View.INVISIBLE);
                editText_add_player.setVisibility(View.INVISIBLE);
                button_add_player.setVisibility(View.INVISIBLE);
                button_start.setVisibility(View.INVISIBLE);
                break;
            case FINISH:
                fragmentFinish();
                break;
        }
    }

    private void selectionAction(){
        Game.getInstance().setGameStatus(GameStatus.PAUSE);
        switch(Game.getInstance().getActionSelected().getClass().getName()){
            case "com.example.picoloapp.Metier.GameManager.Action.Challenge":
                try {
                    ((IFragmentChallenge) getActivity()).fragmentChallenge();
                } catch (ClassCastException e) { }
                break;
            case "com.example.picoloapp.Metier.GameManager.Action.Question":
                Game.getInstance().setGameStatus(GameStatus.RUN);
                break;
            case "com.example.picoloapp.Metier.GameManager.Action.Pledge":
                try {
                    ((IFragmentPledge) getActivity()).fragmentPledge();
                } catch (ClassCastException e) { }
                break;
            default:
                break;
        }
    }

    private void nextAction() {
        clearError();
        switch(Game.getInstance().getGameStatus()){
            case RUN:
                Game.getInstance().nextAction();
                selectionAction();
                break;
            case PAUSE:
                writeError(getString(R.string.game_status_pause));
                break;
            default:
                break;
        }
    }

    private void clearError(){
        textView_error.setText("");
    }

    private void writeError(String error){
        textView_error.setText(error);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    private void fragmentScore() {
        try {
            ((IFragmentScore) getActivity()).fragmentScore();
        } catch (ClassCastException e) { }
    }

    private void fragmentFinish(){
        try {
            ((IFragmentFinish) getActivity()).fragmentFinish();
        } catch (ClassCastException e) { }
    }

    private void fragmentLastGame(){
        try {
            ((IFragmentLastGame) getActivity()).fragmentLastGame();
        } catch (ClassCastException e) { }
    }
}
