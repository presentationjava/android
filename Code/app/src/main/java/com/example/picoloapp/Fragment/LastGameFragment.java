package com.example.picoloapp.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.Metier.GameManager.Player;
import com.example.picoloapp.R;
import com.example.picoloapp.Utils.Adapter.LastGameAdapter;
import com.example.picoloapp.Utils.StaticValues;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class LastGameFragment extends Fragment {
    private RecyclerView.Adapter playerAdapter;
    private TextView textView_score_error;

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_last_game, container, false);

        Game.getInstance().sortListPlayer();

        TextView textView_name_game = view.findViewById(R.id.textView_last_name);
        textView_score_error = view.findViewById(R.id.textView_score_error);

        load();

        textView_name_game.setText(textView_name_game.getText() + Game.getInstance().getGameSave().getName());

        RecyclerView recyclerView_score = view.findViewById(R.id.recycleView_last_game);
        recyclerView_score.setHasFixedSize(true);

        recyclerView_score.setLayoutManager(new LinearLayoutManager(getActivity()));

        playerAdapter = new LastGameAdapter();

        recyclerView_score.setAdapter(playerAdapter);

        return view;
    }

    private void load() {
        SharedPreferences sharedPreferencesName = getContext().getSharedPreferences(StaticValues.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferencesPlayers = getContext().getSharedPreferences(StaticValues.SHARED_PREFERENCES_PLAYERS, Context.MODE_PRIVATE);
        Gson gson = new Gson();

        String jsonName = sharedPreferencesName.getString("name", null);
        String jsonPlayers = sharedPreferencesPlayers.getString("players", null);

        if(jsonName != null)
            Game.getInstance().getGameSave().setName(jsonName);
        if(jsonPlayers != null)
            Game.getInstance().getGameSave().setPlayers((ArrayList<Player>) gson.fromJson(jsonPlayers, new TypeToken<ArrayList<Player>>() {}.getType()));

        if(Game.getInstance().getGameSave().getPlayers() == null || Game.getInstance().getGameSave().getPlayers().size() == 0){
            textView_score_error.setText(getString(R.string.finish_no_game));
        }
    }
}
