package com.example.picoloapp.Fragment;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentGame;
import com.example.picoloapp.Metier.BluetoothClient;
import com.example.picoloapp.Metier.BluetoothManager;
import com.example.picoloapp.Metier.BluetoothServer;
import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.R;
import com.example.picoloapp.Utils.StaticValues;

public class MenuFragment extends Fragment {
    private Activity activity;
    private BluetoothManager bluetoothManager;
    private BluetoothDevice device;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_ENABLE_VISIBILITY = 2;
    private TextView erreurTextView, bluetoothTextView;
    private int tentativeConnection;

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try{
            creatorMenu(view);
            bluetoothInitialisation();
        }catch (Exception e){}
    }

    private void creatorMenu(View view) {
        final Button button_creer = view.findViewById(R.id.button_creer);
        final Button button_rejoindre = view.findViewById(R.id.button_rejoindre);
        final Button button_device_list = view.findViewById(R.id.button_device_list);
        final Button button_start_game = view.findViewById(R.id.button_start_game);
        final EditText editText_game = view.findViewById(R.id.editText_game);
        erreurTextView = view.findViewById(R.id.textView_error);
        bluetoothTextView = view.findViewById(R.id.textView_bluetooth_message);
        button_creer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothServer bluetoothServer = new BluetoothServer();
                bluetoothServer.run();
            }
        });
        button_rejoindre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                device = bluetoothManager.listeAppareilBluetoothConnected();
                if(device != null){
                    BluetoothClient bluetoothClient = new BluetoothClient(device);
                    bluetoothClient.run();
                }
            }
        });
        button_device_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        button_start_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().setName(editText_game.getText().toString());
                fragmentGame();
            }
        });
        effacerMessageErreur();
        setVisibleMessage(true);
    }

    private void bluetoothInitialisation() {
        bluetoothManager = new BluetoothManager(getContext());
        if(!bluetoothManager.isEtatBluetooth()){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    private void rendreBluetoothVisible(){
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivityForResult(discoverableIntent, REQUEST_ENABLE_VISIBILITY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case REQUEST_ENABLE_BT:
                onBluetoothResult(resultCode);
                break;
            case REQUEST_ENABLE_VISIBILITY:
                onVisibilityResult(resultCode);
                break;
        }
    }

    private void onBluetoothResult(int resultCode){
        String logMessage, textMessage;
        switch(resultCode){
            case Activity.RESULT_OK:
                logMessage = getString(R.string.debug_result_ok);
                Log.d(StaticValues.TAG, logMessage);
                bluetoothManager.listeAppareilbluetooth();
                setVisibleMessage(false);
                effacerMessageErreur();
                rendreBluetoothVisible();
                tentativeRemiseZero();
                break;
            case Activity.RESULT_CANCELED:
                tentativeConnection++;
                logMessage = getString(R.string.error_result_cancelled);
                logMessage += tentativeConnection;
                Log.e(StaticValues.TAG, logMessage);
                if(tentativeConnection < 3){
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                    textMessage = getString(R.string.menu_bluetooth_disable) + tentativeConnection;
                    afficherMessageErreur(textMessage);
                }
                if(tentativeConnection == 3){
                    textMessage = getString(R.string.menu_bluetooth_attempts_use);
                    afficherMessageErreur(textMessage);
                }
            default:
                logMessage = getString(R.string.error_result_unknown);
                Log.d(StaticValues.TAG, logMessage);
                break;
        }
    }

    private void onVisibilityResult(int resultCode){
        String logMessage, textMessage;
        switch(resultCode){
            case Activity.RESULT_CANCELED:
                tentativeConnection++;
                logMessage = getString(R.string.error_visibility_cancelled);
                logMessage += tentativeConnection;
                Log.e(StaticValues.TAG, logMessage);
                if(tentativeConnection < 2){
                    rendreBluetoothVisible();
                    textMessage = getString(R.string.menu_visibility_cancelled);
                    afficherMessageErreur(textMessage);
                }
                if(tentativeConnection == 2){
                    logMessage = getString(R.string.debug_visibility_off);
                    textMessage = getString(R.string.menu_visibility_attempts_used);
                    Log.e("com.example.picoloapp", logMessage);
                    afficherMessageErreur(textMessage);
                }
                break;
            case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE: //L'appareil est visible
                logMessage = getString(R.string.debug_result_ok);
                Log.d("com.example.picoloapp", logMessage);
                effacerMessageErreur();
                tentativeRemiseZero();
                break;
            case BluetoothAdapter.SCAN_MODE_CONNECTABLE: //L'appareil est invisible mais peut recevoir des connexions
                logMessage = getString(R.string.debug_visibility_off);
                Log.d("com.example.picoloapp", logMessage);
                effacerMessageErreur();
                tentativeRemiseZero();
                break;
            case Activity.RESULT_OK:
                effacerMessageErreur();
                tentativeRemiseZero();
                break;
            case BluetoothAdapter.SCAN_MODE_NONE: //L'appareil est invisible et ne peut pas recevoir de connexions
                getActivity().finish();
                break;
        }
    }

    private void afficherMessageErreur(String message){
        erreurTextView.setText(message);
    }

    private void effacerMessageErreur(){
        erreurTextView.setText("");
    }

    private void setVisibleMessage(boolean visible){
        if(visible)
            bluetoothTextView.setVisibility(View.VISIBLE);
        else
            bluetoothTextView.setVisibility(View.INVISIBLE);
    }

    private void tentativeRemiseZero(){
        tentativeConnection = 0;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    private void fragmentGame() {
        try {
            ((IFragmentGame) activity).fragmentGame();
        } catch (ClassCastException e) { }
    }
}
