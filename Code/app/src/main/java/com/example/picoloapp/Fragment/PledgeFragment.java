package com.example.picoloapp.Fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.picoloapp.Bibliotheque.Interfaces.IFragmentGame;
import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.Metier.GameManager.Action.Pledge;
import com.example.picoloapp.R;
import com.example.picoloapp.Utils.Enumeration.GameStatus;

public class PledgeFragment extends Fragment {
    private EditText editText_pledge;

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pledge, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        creationId();
    }

    private void creationId() {
        final TextView textView_title = getView().findViewById(R.id.textView_pledge_title);
        editText_pledge = getView().findViewById(R.id.editText_pledge_player);
        final Button button_valid = getView().findViewById(R.id.button_pledge_valid);
        textView_title.setText(textView_title.getText() + Game.getInstance().getPlayerSelected().getName());
        button_valid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validationPledge();
            }
        });
    }

    private void validationPledge() {
        if(TextUtils.isEmpty(editText_pledge.getText().toString())){
            writeError(getString(R.string.pledge_empty_description));
            return;
        }
        Pledge pledge = (Pledge) Game.getInstance().getActionSelected();
        pledge.setPlayer(Game.getInstance().getPlayerSelected());
        pledge.setPledge(editText_pledge.getText().toString());
        Game.getInstance().getListPledge().add(pledge);
        closeFragment();
    }

    private void writeError(String error){
        final TextView textView = getView().findViewById(R.id.textView_error);
        textView.setText(error);
    }

    private void closeFragment() {
        try {
            Game.getInstance().setGameStatus(GameStatus.RUN);
            Game.getInstance().isFinish();
            ((IFragmentGame) getActivity()).fragmentGame();
        } catch (ClassCastException e) { }

    }
}
