package com.example.picoloapp.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.R;
import com.example.picoloapp.Utils.Adapter.PlayerAdapter;
import com.example.picoloapp.Utils.Adapter.PledgeAdapter;

public class ScoreFragment extends Fragment {
    private RecyclerView.Adapter playerAdapter;
    private RecyclerView.Adapter pledgeAdapter;
    private TextView textView_score_error;
    private TextView textView_pledge_error;

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_score, container, false);

        Game.getInstance().sortListPlayer();

        TextView textView_name_game = view.findViewById(R.id.textView_name_score);
        TextView textView_name_pledge = view.findViewById(R.id.textView_name_pledge);
        textView_score_error = view.findViewById(R.id.textView_score_error);
        textView_pledge_error = view.findViewById(R.id.textView_pledge_error);

        listsAreEmpty();

        textView_name_game.setText(textView_name_game.getText() + Game.getInstance().getName());
        textView_name_pledge.setText(textView_name_pledge.getText() + Game.getInstance().getName());

        RecyclerView recyclerView_score = view.findViewById(R.id.recycleView_score);
        RecyclerView recyclerView_pledge = view.findViewById(R.id.recycleView_pledge);
        recyclerView_score.setHasFixedSize(true);
        recyclerView_pledge.setHasFixedSize(true);

        recyclerView_score.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView_pledge.setLayoutManager(new LinearLayoutManager(getActivity()));

        playerAdapter = new PlayerAdapter();
        pledgeAdapter = new PledgeAdapter();

        recyclerView_score.setAdapter(playerAdapter);
        recyclerView_pledge.setAdapter(pledgeAdapter);

        return view;
    }

    private void listsAreEmpty(){
        if(Game.getInstance().getListPlayer().size() == 0){
            textView_score_error.setText(getString(R.string.score_no_player) + Game.getInstance().getName() + " !");
        }
        if(Game.getInstance().getListPledge().size() == 0){
            textView_pledge_error.setText(getString(R.string.score_no_pledge) + Game.getInstance().getName() + " !");
        }
    }
}
