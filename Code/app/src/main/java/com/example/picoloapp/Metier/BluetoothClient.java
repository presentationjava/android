package com.example.picoloapp.Metier;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.example.picoloapp.Bibliotheque.Interfaces.IBluetoothConnection;
import com.example.picoloapp.Utils.StaticValues;

import java.io.IOException;

public class BluetoothClient extends Thread implements IBluetoothConnection {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothCommunication bluetoothCommunication;

    public BluetoothClient(BluetoothDevice device) {
        // Use a temporary object that is later assigned to mmSocket
        // because mmSocket is final.
        BluetoothSocket tmp = null;
        mmDevice = device;

        try {
            // Get a BluetoothSocket to connect with the given BluetoothDevice.
            // MY_UUID is the app's UUID string, also used in the server code.
            tmp = device.createRfcommSocketToServiceRecord(StaticValues.MY_UUID);
        } catch (IOException e) {
            Log.e(StaticValues.TAG, e.getCause() + " -> " + e.getMessage());
        }
        mmSocket = tmp;
    }

    public void run() {
        // Cancel discovery because it otherwise slows down the connection.
        bluetoothAdapter.cancelDiscovery();

        try {
            // Connect to the remote device through the socket. This call blocks
            // until it succeeds or throws an exception.
            mmSocket.connect();
        } catch (IOException connectException) {
            // Unable to connect; close the socket and return.
            try {
                mmSocket.close();
            } catch (IOException closeException) {
                Log.e(StaticValues.TAG, closeException.getCause() + " -> " + closeException.getMessage());
            }
            return;
        }

        // The connection attempt succeeded. Perform work associated with
        // the connection in a separate thread.
        manageMyConnectedSocket(mmSocket);
    }

    // Closes the client socket and causes the thread to finish.
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e(StaticValues.TAG, e.getCause() + " -> " + e.getMessage());
        }
    }

    public void manageMyConnectedSocket(BluetoothSocket socket) {
        bluetoothCommunication = new BluetoothCommunication(socket);
        bluetoothCommunication.start();
    }
}
