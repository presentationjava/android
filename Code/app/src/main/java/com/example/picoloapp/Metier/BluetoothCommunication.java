package com.example.picoloapp.Metier;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.example.picoloapp.Utils.StaticValues;

import java.io.IOException;

public class BluetoothCommunication extends Thread {
    private BluetoothSocket socket;
    private final String balise = "[Bluetooth] -> ";

    public BluetoothCommunication(BluetoothSocket socket){
        this.socket = socket;
    }

    public void run(){
        int i;
        String message = "";
        try {
            while((i = socket.getInputStream().read()) != -1){
                Log.d(StaticValues.TAG,balise + i);
                message += (char)i;
                Log.d(StaticValues.TAG,balise  + (char)i + "\n\tMessage : " + message);
            }
        } catch (IOException e) {
            Log.e(StaticValues.TAG, balise, e);
        }
        Log.d(StaticValues.TAG,balise + message);
    }
}
