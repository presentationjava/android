package com.example.picoloapp.Metier;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.example.picoloapp.R;
import com.example.picoloapp.Utils.StaticValues;

import java.util.Set;

public class BluetoothManager {
    private BluetoothAdapter bluetoothAdapter;
    private boolean etatBluetooth = false;
    private Set<BluetoothDevice> listeBluetoothDevice;

    public BluetoothManager(Context context){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        context.registerReceiver(stateChangeReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        context.registerReceiver(actionFoundReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        if (bluetoothAdapter == null) {
            String message = context.getString(R.string.error_support_bluetooth);
            Log.e(StaticValues.TAG, message);
            System.exit(1);
        }
        else {
            String message = context.getString(R.string.debug_support_bluetooth);
            Log.d(StaticValues.TAG, message);
        }

    }

    public boolean isEtatBluetooth() {
        return etatBluetooth;
    }

    public void listeAppareilbluetooth(){
        listeBluetoothDevice = bluetoothAdapter.getBondedDevices();

        if (listeBluetoothDevice.size() > 0) {
            for (BluetoothDevice device : listeBluetoothDevice) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress();
                Log.d(StaticValues.TAG, "\t" + deviceName + "\t" + deviceHardwareAddress);
            }
        }
    }

    public BluetoothDevice listeAppareilBluetoothConnected(){
        for(BluetoothDevice device : listeBluetoothDevice){
            if(device.getName().equals(StaticValues.BLUETOOTH_DEVICE_NAME)){
                return device;
            }
        }
        return null;
    }

    private final BroadcastReceiver stateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(bluetoothAdapter.ACTION_STATE_CHANGED.equals(action)){
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);

                switch (state) {
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        //Indicates the local Bluetooth adapter is turning off. Local clients should immediately attempt graceful disconnection of any remote links.
                        break;
                    case BluetoothAdapter.STATE_OFF:
                        etatBluetooth = false;
                        //Indicates the local Bluetooth adapter is off.
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        //Indicates the local Bluetooth adapter is turning on. However local clients should wait for STATE_ON before attempting to use the adapter.
                        break;
                    case BluetoothAdapter.STATE_ON:
                        etatBluetooth = true;
                        //Indicates the local Bluetooth adapter is on, and ready for use.
                        break;
                }
            }
        }
    };

    private final BroadcastReceiver actionFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress();
                Log.d(StaticValues.TAG, deviceName + "\t" + deviceHardwareAddress);
            }
        }
    };
}
