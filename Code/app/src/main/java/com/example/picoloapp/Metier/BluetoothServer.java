package com.example.picoloapp.Metier;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.example.picoloapp.Bibliotheque.Interfaces.IBluetoothConnection;
import com.example.picoloapp.Utils.StaticValues;

import java.io.IOException;

public class BluetoothServer extends Thread implements IBluetoothConnection {
    private final BluetoothServerSocket server;
    private final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    public BluetoothServer(){
        BluetoothServerSocket serverTmp = null;
        try{
            serverTmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(StaticValues.NAME, StaticValues.MY_UUID);
        } catch (IOException e) {
            Log.e(StaticValues.TAG, e.getCause() + " -> " + e.getMessage());
        }
        server = serverTmp;
    }

    public void run() {
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned.
        while (true) {
            try {
                socket = server.accept();
            } catch (IOException e) {
                Log.e(StaticValues.TAG, e.getCause() + " -> " + e.getMessage());
                break;
            }

            if (socket != null) {
                // A connection was accepted. Perform work associated with
                // the connection in a separate thread.
                manageMyConnectedSocket(socket);
                try {
                    server.close();
                } catch (IOException e) {
                    Log.e(StaticValues.TAG, e.getCause() + " -> " + e.getMessage());
                }
                break;
            }
        }
    }

    // Closes the connect socket and causes the thread to finish.
    public void cancel() {
        try {
            server.close();
        } catch (IOException e) {
            Log.e(StaticValues.TAG, e.getCause() + " -> " + e.getMessage());
        }
    }

    public void manageMyConnectedSocket(BluetoothSocket socket){
        String message = "test";
        byte[] send = message.getBytes();
        try {
            socket.getOutputStream().write(send);
            Log.d(StaticValues.TAG, StaticValues.DEBUG_TAG_BT_SERVER + message + "\n\tEnvoyé !");
        } catch (IOException e) {
            Log.e(StaticValues.TAG, e.getCause() + " -> " + e.getMessage());
        }
    }
}
