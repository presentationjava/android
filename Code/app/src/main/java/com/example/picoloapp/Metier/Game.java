package com.example.picoloapp.Metier;

import android.util.Log;

import com.example.picoloapp.Bibliotheque.Interfaces.IAction;
import com.example.picoloapp.Bibliotheque.Interfaces.IStrategiePersistance;
import com.example.picoloapp.Metier.GameManager.Action.Pledge;
import com.example.picoloapp.Metier.GameManager.GameController;
import com.example.picoloapp.Metier.GameManager.GameSave;
import com.example.picoloapp.Metier.GameManager.Player;
import com.example.picoloapp.Utils.Enumeration.GameStatus;
import com.example.picoloapp.Utils.ReturnCode;
import com.example.picoloapp.Utils.StaticValues;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

// Singleton pour avoir qu'une seule game et en plus pour que je puisse le diffuser entre fragment

public class Game {
    private static Game uniqGame;

    private GameStatus gameStatus = GameStatus.NONE;
    private String name = "Game";
    private GameController gameController = new GameController();
    private IStrategiePersistance strategiePersistance;
    private ArrayList<Player> listPlayer;
    private ArrayList<IAction> listAction;
    private ArrayList<Pledge> listPledge;
    private GameSave gameSave = new GameSave();

    private Player playerSelected;
    private IAction actionSelected;

    private Game() {
        this.strategiePersistance = StaticValues.STRATEGIE_PERSISTANCE;
        listPlayer = new ArrayList<Player>();
        listAction = new ArrayList<IAction>();
        listPledge = new ArrayList<Pledge>();
        init();
    }

    public static Game getInstance(){
        if(uniqGame == null)
            uniqGame = new Game();
        return uniqGame;
    }

    public void clearInstance(){
        gameStatus = GameStatus.NONE;
        uniqGame = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        Log.d(StaticValues.TAG, this.gameStatus.name() + " -> " + gameStatus.name());
        this.gameStatus = gameStatus;
    }

    public Player getPlayerSelected() {
        return playerSelected;
    }

    public IAction getActionSelected() {
        return actionSelected;
    }

    public ArrayList<Player> getListPlayer() {
        return listPlayer;
    }

    public ArrayList<IAction> getListAction(){
        return listAction;
    }

    public ArrayList<Pledge> getListPledge(){
        return listPledge;
    }

    public GameSave getGameSave() {
        return gameSave;
    }

    public void setGameSave(GameSave gameSave) {
        this.gameSave = gameSave;
    }

    private void init(){
        loadActions();
        displayPlayers();
        displayActions();
        setGameStatus(GameStatus.INITIALIZATION);
    }

    private void loadActions(){
        listAction = strategiePersistance.loadActions();
    }

    public void sortListPlayer(){
        Collections.sort(listPlayer);
    }

    public boolean addPlayer(String name){
        for(int i=0; i < listPlayer.size(); i++){
            if(name.equalsIgnoreCase(listPlayer.get(i).getName())){
                return false;
            }
        }
        listPlayer.add(new Player(name, 0));
        return true;
    }

    public void startGame(){
        switch(gameController.startCondition()){
            case ReturnCode.SUCCESS:
                setGameStatus(GameStatus.START);
                break;
            case ReturnCode.ERROR_PLAYERS_SIZE:
                setGameStatus(GameStatus.PAUSE);
                return;
            case ReturnCode.ERROR_ACTIONS_SIZE:
                setGameStatus(GameStatus.PAUSE);
                loadActions();
                startGame();
                return;
            default:
                return;
        }
        setGameStatus(GameStatus.RUN);
    }

    private void displayPlayers(){
        String s = "\n";
        for(int i = 0; i < listPlayer.size(); i++){
            s += (listPlayer.get(i).toString() + "\n");
        }
        Log.d(StaticValues.TAG, s);
    }

    private void displayActions(){
        String s = "\n";
        for(int i = 0; i < listAction.size(); i++){
            s += (listAction.get(i).toString() + "\n");
        }
        Log.d(StaticValues.TAG, s);
    }

    private void clearSelectedItems(){
        playerSelected = null;
        actionSelected = null;
    }

    private Player randomPlayer(){
        Random random = new Random();
        return listPlayer.get(random.nextInt(listPlayer.size()));
    }

    private IAction randomAction(){
        Random random = new Random();
        return listAction.get(random.nextInt(listAction.size()));
    }

    public void isFinish(){
        if(listAction.size() == 0){
            setGameStatus(GameStatus.FINISH);
        }
    }

    public void nextAction(){
        clearSelectedItems();
        if(gameStatus != GameStatus.RUN){
            return;
        }
        if(listAction.size() == 0){
            isFinish();
            return;
        }
        actionSelected = randomAction();
        switch(actionSelected.getClass().getName()){
            case "com.example.picoloapp.Metier.GameManager.Action.Challenge":
            case "com.example.picoloapp.Metier.GameManager.Action.Pledge":
                playerSelected = randomPlayer();
                break;
            case "com.example.picoloapp.Metier.GameManager.Action.Question":
                break;
            default:
                break;
        }
        listAction.remove(actionSelected);
    }

    public void save(){
        gameSave.setName(this.name);
        gameSave.setPlayers(this.listPlayer);
    }
}
