package com.example.picoloapp.Metier.GameManager.Action;

import androidx.annotation.NonNull;

public class Answer {
    private String answer;

    public Answer(String answer){
        this.answer = answer;
    }

    @NonNull
    @Override
    public String toString() {
        return answer;
    }
}
