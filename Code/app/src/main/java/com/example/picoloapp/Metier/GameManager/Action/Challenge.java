package com.example.picoloapp.Metier.GameManager.Action;

import com.example.picoloapp.Bibliotheque.Interfaces.IAction;

public class Challenge implements IAction {
    private String description;

    public Challenge(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }
}
