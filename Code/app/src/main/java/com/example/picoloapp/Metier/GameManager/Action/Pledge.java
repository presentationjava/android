package com.example.picoloapp.Metier.GameManager.Action;

import com.example.picoloapp.Bibliotheque.Interfaces.IAction;
import com.example.picoloapp.Metier.GameManager.Player;

public class Pledge implements IAction{
    private Player player = null;
    private String pledge = null;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getPledge() {
        return pledge;
    }

    public void setPledge(String pledge) {
        this.pledge = pledge;
    }
}
