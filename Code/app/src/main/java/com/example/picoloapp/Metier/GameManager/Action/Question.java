package com.example.picoloapp.Metier.GameManager.Action;

import androidx.annotation.NonNull;

import com.example.picoloapp.Bibliotheque.Interfaces.IAction;

import java.util.ArrayList;

public class Question implements IAction {
    private String question;
    private ArrayList<Answer> listAnswer;

    public Question(String question){
        this.question = question;
        listAnswer = new ArrayList<Answer>();
    }

    public void addAnswer(String answer){
        listAnswer.add(new Answer(answer));
    }

    public void removeAnswer(Answer answer){
        listAnswer.remove(answer);
    }

    @NonNull
    @Override
    public String toString() {
        String s = question;
        for(int i = 0; i < listAnswer.size(); i++){
            s += ("\n\t" + listAnswer.get(i).toString());
        }
        return s;
    }
}
