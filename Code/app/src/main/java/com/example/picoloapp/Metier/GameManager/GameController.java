package com.example.picoloapp.Metier.GameManager;

import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.Utils.ReturnCode;
import com.example.picoloapp.Utils.StaticValues;

public class GameController {
    public int startCondition(){
        if(Game.getInstance().getListPlayer().size() < StaticValues.PLAYERS_MINIMUM){
            return ReturnCode.ERROR_PLAYERS_SIZE;
        }
        if(Game.getInstance().getListAction().size() < StaticValues.ACTIONS_MINIMUM){
            return ReturnCode.ERROR_ACTIONS_SIZE;
        }
        return ReturnCode.SUCCESS;
    }
}
