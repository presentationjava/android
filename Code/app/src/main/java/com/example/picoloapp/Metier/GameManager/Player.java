package com.example.picoloapp.Metier.GameManager;

import androidx.annotation.NonNull;

public class Player implements Comparable {
    private String name;
    private int score;

    public Player(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        if(score < 0){
            this.score = 0;
            return;
        }
        this.score = score;
    }

    @NonNull
    @Override
    public String toString() {
        return name + " -> " + score;
    }

    @Override
    public int compareTo(Object o) {
        if(o.getClass().equals(this.getClass())){
            Player p = (Player) o;
            return p.getScore() - this.score;
        }
        return 0;
    }
}
