package com.example.picoloapp.Utils.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.R;

public class LastGameAdapter extends RecyclerView.Adapter<LastGameAdapter.LastGameViewHolder> {
    @NonNull
    @Override
    public LastGameAdapter.LastGameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.score_cell, parent, false);
        return new LastGameAdapter.LastGameViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull LastGameAdapter.LastGameViewHolder holder, int position) {
        holder.textViewPseudo.setText(Game.getInstance().getGameSave().getPlayers().get(position).getName());
        holder.textViewScore.setText(String.valueOf(Game.getInstance().getGameSave().getPlayers().get(position).getScore()));
    }

    @Override
    public int getItemCount() {
        if(Game.getInstance().getGameSave().getPlayers() == null)
            return 0;
        return Game.getInstance().getGameSave().getPlayers().size();
    }

    public static class LastGameViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewPseudo;
        public TextView textViewScore;

        public LastGameViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewPseudo = itemView.findViewById(R.id.textView_pseudo);
            textViewScore = itemView.findViewById(R.id.textView_score);
        }
    }
}
