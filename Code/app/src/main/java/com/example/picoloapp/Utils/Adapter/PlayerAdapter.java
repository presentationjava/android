package com.example.picoloapp.Utils.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.R;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.PlayerViewHolder> {
    @NonNull
    @Override
    public PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.score_cell, parent, false);
        return new PlayerViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerViewHolder holder, int position) {
        holder.textViewPseudo.setText(Game.getInstance().getListPlayer().get(position).getName());
        holder.textViewScore.setText(String.valueOf(Game.getInstance().getListPlayer().get(position).getScore()));
    }

    @Override
    public int getItemCount() {
        return Game.getInstance().getListPlayer().size();
    }

    public static class PlayerViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewPseudo;
        public TextView textViewScore;

        public PlayerViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewPseudo = itemView.findViewById(R.id.textView_pseudo);
            textViewScore = itemView.findViewById(R.id.textView_score);
        }
    }
}
