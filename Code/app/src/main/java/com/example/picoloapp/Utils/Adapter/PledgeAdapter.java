package com.example.picoloapp.Utils.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.picoloapp.Metier.Game;
import com.example.picoloapp.R;

public class PledgeAdapter extends RecyclerView.Adapter<PledgeAdapter.PledgeViewHolder> {
    @NonNull
    @Override
    public PledgeAdapter.PledgeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.pledge_cell, parent, false);
        return new PledgeAdapter.PledgeViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull PledgeAdapter.PledgeViewHolder holder, int position) {
        holder.textViewPseudo.setText(Game.getInstance().getListPledge().get(position).getPlayer().getName());
        holder.textViewPledge.setText(Game.getInstance().getListPledge().get(position).getPledge());
    }

    @Override
    public int getItemCount() {
        return Game.getInstance().getListPledge().size();
    }

    public static class PledgeViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewPseudo;
        public TextView textViewPledge;

        public PledgeViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewPseudo = itemView.findViewById(R.id.textView_pseudo);
            textViewPledge = itemView.findViewById(R.id.textView_pledge);
        }
    }
}
