package com.example.picoloapp.Utils.Enumeration;

public enum GameStatus {
    INITIALIZATION,
    START,
    RUN,
    PAUSE,
    FINISH,
    NONE
}
