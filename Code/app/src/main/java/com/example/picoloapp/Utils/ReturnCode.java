package com.example.picoloapp.Utils;

public class ReturnCode {
    // Code retour pour lancement partie
    public static final int SUCCESS = 0; // Tout c'est bien passé
    public static final int ERROR_PLAYERS_SIZE = 1; // Liste de player < 2
    public static final int ERROR_ACTIONS_SIZE = 2; // Liste d'action < 5
}
