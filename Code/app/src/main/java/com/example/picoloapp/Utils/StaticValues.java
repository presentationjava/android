package com.example.picoloapp.Utils;

import com.example.picoloapp.Bibliotheque.Interfaces.IStrategiePersistance;
import com.example.picoloapp.Bibliotheque.Persistence.STUB;

public class StaticValues {
    // Gestion des Tags
    public static final String TAG = "picoloapp";
    public static final String DEBUG_TAG_BT_SERVER = "[BluetoothServer]: ";
    public static final String DEBUG_TAG_BT_MANAGER = "[BluetoothManager]: ";

    // Strategie Persistance
    public static final IStrategiePersistance STRATEGIE_PERSISTANCE = new STUB();

    // Gestion de listenUsingInsecureRfcommWithServiceRecord (qui prend un name et un UUID)
    public static final String NAME = "thvincentBluetooth";
    public static final java.util.UUID MY_UUID = java.util.UUID.fromString("DEADBEEF-0000-0000-0000-000000000000");

    // Parametrage partie
    public static final int PLAYERS_MINIMUM = 2; // Nombre de joueur minimum pour lancer la partie
    public static final int ACTIONS_MINIMUM = 5; // Nombre d'action minimum pour lancer la partie

    // Parametrage score
    public static final int SCORE_CHALLENGE_VALID = 1;
    public static final int SCORE_CHALLENGE_FAILED = -1;

    // Parametre sauvegarde
    public static final String SHARED_PREFERENCES_NAME = "NAME";
    public static final String SHARED_PREFERENCES_PLAYERS = "PLAYERS";

    // Parametre Bluetooth
    public static final String BLUETOOTH_DEVICE_NAME = "OnePlus 7T Pro";
}
